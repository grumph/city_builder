#ifndef GAME_STATE_MENU_HPP
#define GAME_STATE_MENU_HPP

#include <SFML/Graphics.hpp>
#include "game_state.hpp"


class GameStateMenu : public GameState
{
private:
    sf::View view;
    void start_game();

public:
    virtual void draw(const float deltatime);
    virtual void update(const float deltatime);
    virtual void handleInput();

    GameStateMenu(Game* game);
};


#endif // GAME_STATE_MENU_HPP
