#ifndef GAME_STATE_GAMEBOARD_HPP
#define GAME_STATE_GAMEBOARD_HPP

#include <SFML/Graphics.hpp>

#include "game_state.hpp"
#include "map.hpp"

enum class ActionState { NONE, PANNING, SELECTING };

class GameStateGameboard : public GameState
{
private:
    ActionState actionState;
    sf::View gameView;
    sf::View guiView;
    Map map;
    sf::Vector2i panningAnchor;
    float zoomLevel;

    sf::Vector2i selectionStart;
    sf::Vector2i selectionEnd;
    Tile* currentTile;

public:
    virtual void draw(const float deltatime);
    virtual void update(const float deltatime);
    virtual void handleInput();

    GameStateGameboard(Game* game);
};


#endif // GAME_STATE_GAMEBOARD_HPP
