#ifndef ANIMATION_HANDLER_HPP
#define ANIMATION_HANDLER_HPP

#include <SFML/Graphics.hpp>
#include <vector>

class Animation
{
public:
    unsigned int startFrame;
    unsigned int endFrame;
    float duration;

    unsigned int getLength();

    Animation(unsigned int startFrame, unsigned int endFrame, float duration);
};


class AnimationHandler
{
private:
    std::vector<Animation> animations; // Array of animations
    float t; // Current time since the animation loop started
    int currentAnim;

public:
    /* Add a new animation */
    void addAnim(Animation& anim);

    /* Update the current frame of animation. deltatime is the time since
     * the update was last called (i.e. the time for one frame to be
     * executed) */
    void update(const float deltatime);

    /* Change the animation, resetting t in the process */
    void changeAnim(unsigned int animNum);

    /* Current section of the texture that should be displayed */
    sf::IntRect bounds;

    /* Pixel dimensions of each individual frame */
    sf::IntRect frameSize;

    /* Constructor */
    AnimationHandler();
    AnimationHandler(const sf::IntRect& frameSize);
};


#endif // ANIMATION_HANDLER_HPP
