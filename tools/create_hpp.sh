#!/bin/bash


if [ -z "$1" ]; then
    echo "Usage : $0 file.hpp"
    echo "will create include/file.hpp with correct #ifndef inside"
    echo "include/ is automatically added"
    exit 1
fi

if [ ! -d "include" ]; then
    echo "Launch script from project root folder"
    exit 1
fi

if [ "${1%.hpp}" == "${1}" ]; then
    FILE_NAME="${1}.hpp"
else
    FILE_NAME="${1}"
fi

FILE="include/${FILE_NAME}"

if [ -e "$FILE" ]; then
    echo "File exists !"
    exit 1
fi

INCLUDE="$( echo "${FILE_NAME}" | tr '.-/' '_' | tr '[:lower:]' '[:upper:]' )"
echo "#ifndef ${INCLUDE}
#define ${INCLUDE}



#endif // ${INCLUDE}" >  ${FILE}
