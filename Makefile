


BINARY := ./bin/main

SRC_DIR := ./src
BUILD_DIR := ./build
INCLUDE_DIR := ./include

SRC := $(shell find $(SRC_DIR) -type f -name '*.cpp')
OBJ := $(patsubst $(SRC_DIR)/%.cpp, $(BUILD_DIR)/%.obj, $(SRC))

CC = clang
CFLAGS = -Wall -Wextra -I$(INCLUDE_DIR) -std=c++11
LDFLAGS := -lsfml-graphics -lsfml-window -lsfml-system -lstdc++


all: $(BINARY)


$(BUILD_DIR)/%.obj: $(SRC_DIR)/%.cpp
	mkdir -p $(BUILD_DIR)/$(dir $*)
	$(CC) -c -o $@ $< $(CFLAGS)


$(BINARY): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)


debug:
	$(CC) -g $(SRC) -o $(BINARY) $(CFLAGS) $(LDFLAGS)


run: $(BINARY)
	$(BINARY)


.PHONY: clean
clean:
	rm -rf $(BINARY) $(BUILD_DIR)/*

