#include "animation_handler.hpp"

unsigned int Animation::getLength()
{
    return endFrame - startFrame + 1;
}


Animation::Animation(unsigned int startFrame, unsigned int endFrame, float duration)
{
    this->startFrame = startFrame;
    this->endFrame = endFrame;
    this->duration = duration;
}



AnimationHandler::AnimationHandler()
{
    this->t = 0.0f;
    this->currentAnim = -1;
}



AnimationHandler::AnimationHandler(const sf::IntRect& frameSize)
{
    this->frameSize = frameSize;

    this->t = 0.0f;
    this->currentAnim = -1;
}


void AnimationHandler::update(const float deltatime)
{
    if(currentAnim >= this->animations.size() || currentAnim < 0)
        return;

    float duration = this->animations[currentAnim].duration;

    /* Check if the animation has progessed to a new frame and if so
     * change to the next frame */
    if(int((t + deltatime) / duration) > int(t / duration))
    {
        /* Calculate the frame number */
        int frame = int((t + deltatime) / duration);

        /* Adjust for looping */
        frame %= this->animations[currentAnim].getLength();

        /* Set the sprite to the new frame */
        sf::IntRect rect = this->frameSize;
        rect.left = rect.width * frame;
        rect.top = rect.height * this->currentAnim;
        this->bounds = rect;
    }

    /* Increment the time elapsed */
    this->t += deltatime;
    /* Adjust for looping */
    if(this->t > duration * this->animations[currentAnim].getLength())
    {
        this->t = 0.0f;
    }
}


void AnimationHandler::addAnim(Animation& anim)
{
    this->animations.push_back(anim);
}

void AnimationHandler::changeAnim(unsigned int animID)
{
    /* Do not change the animation if the animation is currently active or
     * the new animation does not exist */
    if(this->currentAnim == animID || animID >= this->animations.size() ||
       animID < 0)
        return;

    /* Set the current animation */
    this->currentAnim = animID;
    /* Update the animation bounds */
    sf::IntRect rect = this->frameSize;
    rect.top = rect.height * animID;
    this->bounds = rect;
    this->t = 0.0;
}
