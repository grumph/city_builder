
#include <SFML/System.hpp>

#include "game.hpp"
#include "game_state.hpp"

#include "animation_handler.hpp"

void Game::loadTextures()
{

#define RES_FOLDER "res/debug/"

    this->texmgr.loadTexture("background", "res/background.png");
    this->texmgr.loadTexture("grass",      RES_FOLDER"grass.png");
    this->texmgr.loadTexture("forest",     RES_FOLDER"forest.png");
    this->texmgr.loadTexture("water",      RES_FOLDER"water.png");
    this->texmgr.loadTexture("residential",RES_FOLDER"residential.png");
    this->texmgr.loadTexture("commercial", RES_FOLDER"commercial.png");
    this->texmgr.loadTexture("industrial", RES_FOLDER"industrial.png");
    this->texmgr.loadTexture("road",       RES_FOLDER"roads.png");
}

void Game::pushState(GameState* state)
{
    this->states.push(state);
}


void Game::popState()
{
    delete this->states.top();
    this->states.pop();
}


void Game::changeState(GameState* state)
{
    if(!this->states.empty())
        popState();
    pushState(state);
}


GameState* Game::peekState()
{
    if(this->states.empty())
        return nullptr;
    else
        return this->states.top();
}


void Game::gameLoop()
{
    sf::Clock clock;

    while(this->window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        float delta_time = elapsed.asSeconds();

        if(peekState() == nullptr)
            continue;

        peekState()->handleInput();
        peekState()->update(delta_time);
        this->window.clear(sf::Color::Black);
        peekState()->draw(delta_time);
        this->window.display();
    }
}


Game::Game()
{
    this->loadTextures();
    this->loadTiles();
    this->window.create(sf::VideoMode(800, 600), "Tropicroutes");
    this->window.setFramerateLimit(60);
    this->background.setTexture(this->texmgr.getRef("background"));
}


Game::~Game()
{
    while(!this->states.empty())
        popState();
}


void Game::loadTiles()
{
    Animation staticAnim(0, 0, 1.0f);
    this->tileAtlas["grass"] =
        Tile(this->tileSize, 1, texmgr.getRef("grass"),
             { staticAnim },
             TileType::GRASS, 50, 0, 1);
    this->tileAtlas["forest"] =
        Tile(this->tileSize, 1, texmgr.getRef("forest"),
             { staticAnim },
             TileType::FOREST, 100, 0, 1);
    this->tileAtlas["water"] =
        Tile(this->tileSize, 1, texmgr.getRef("water"),
             { Animation(0, 3, 0.5f),
                     Animation(0, 3, 0.5f),
                     Animation(0, 3, 0.5f) },
             TileType::WATER, 0, 0, 1);
    this->tileAtlas["residential"] =
        Tile(this->tileSize, 2, texmgr.getRef("residential"),
             { staticAnim, staticAnim, staticAnim,
                     staticAnim, staticAnim, staticAnim },
             TileType::RESIDENTIAL, 300, 50, 6);
    this->tileAtlas["commercial"] =
        Tile(this->tileSize, 2, texmgr.getRef("commercial"),
             { staticAnim, staticAnim, staticAnim, staticAnim},
             TileType::COMMERCIAL, 300, 50, 4);
    this->tileAtlas["industrial"] =
        Tile(this->tileSize, 2, texmgr.getRef("industrial"),
             { staticAnim, staticAnim, staticAnim,
                     staticAnim },
             TileType::INDUSTRIAL, 300, 50, 4);
    this->tileAtlas["road"] =
        Tile(this->tileSize, 1, texmgr.getRef("road"),
             { staticAnim, staticAnim, staticAnim,
                     staticAnim, staticAnim, staticAnim,
                     staticAnim, staticAnim, staticAnim,
                     staticAnim, staticAnim },
             TileType::ROAD, 100, 0, 1);
}
