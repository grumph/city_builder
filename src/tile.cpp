

#include "tile.hpp"


Tile::Tile()
{
}

Tile::Tile(const unsigned int tileSize, const unsigned int height, sf::Texture& texture,
           const std::vector<Animation>& animations,
           const TileType tileType, const unsigned int cost, const unsigned int maxPopPerLevel,
           const unsigned int maxLevels)
{
    this->tileType = tileType;
    this->tileVariant = 0;
    this->regions[0] = 0;

    this->cost = cost;
    this->population = 0;
    this->maxPopPerLevel = maxPopPerLevel;
    this->maxLevels = maxLevels;
    this->production = 0;
    this->storedGoods = 0;

    this->sprite.setOrigin(sf::Vector2f(0.0f, tileSize*(height-1)));
    this->sprite.setTexture(texture);
    this->animHandler.frameSize = sf::IntRect(0, 0, tileSize*2 - 2, tileSize*height);
    for(auto animation : animations)
    {
        this->animHandler.addAnim(animation);
    }
    this->animHandler.update(0.0f);
}


std::string Tile::getCost()
{
    return std::to_string(this->cost);
}




void Tile::draw(sf::RenderWindow& window, float deltatime)
{
    /* Change the sprite to reflect the tile variant */
    this->animHandler.changeAnim(this->tileVariant);

    /* Update the animation */
    this->animHandler.update(deltatime);

    /* Update the sprite */
    this->sprite.setTextureRect(this->animHandler.bounds);

    /* Draw the tile */
    window.draw(this->sprite);

    return;
}

void Tile::update()
{
    /* If the population is at the maximum value for the tile,
     * there is a small chance that the tile will increase its
     * building stage */
    if((this->tileType == TileType::RESIDENTIAL ||
        this->tileType == TileType::COMMERCIAL ||
        this->tileType == TileType::INDUSTRIAL) &&
       this->population == this->maxPopPerLevel * (this->tileVariant+1) &&
       this->tileVariant < this->maxLevels)
    {
        if(rand() % int(1e4) < 1e2 / (this->tileVariant+1)) ++this->tileVariant;
    }

    return;
}

std::string tileTypeToStr(TileType type)
{
    switch(type)
    {
    default:
    case TileType::VOID:            return "Void";
    case TileType::GRASS:           return "Flatten";
    case TileType::FOREST:          return "Forest";
    case TileType::WATER:           return "Water";
    case TileType::RESIDENTIAL:     return "Residential Zone";
    case TileType::COMMERCIAL:      return "Commercial Zone";
    case TileType::INDUSTRIAL:      return "Industrial Zone";
    }
}
